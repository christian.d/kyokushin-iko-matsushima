$(document).ready(()=>{
	function navScrollBehavior(event){
		var scrollPosition = $(window).scrollTop();
		if(scrollPosition >= 28){ // when scrolled 28 pixels away from the top 
			$('.nav-bg').css('position', 'fixed');
			$('.nav-bg').css('z-index',99);
			$('.header-background').css('padding-top',0);
			$('.nav-bg').css('width','100%');
			$('.nav-bg').css('transition','all 1s ease');
			$('.nav-bg').css('margin-left',0);
		}

		else if(scrollPosition < 28){ // when scrolled below 28 pixels from the top 
			$('.nav-bg').removeAttr('style');
			$('.header-background').css('padding-top','2%');
			$('.nav-bg').css('width','90%');
		}
	}

	$(window).scroll(navScrollBehavior);
});